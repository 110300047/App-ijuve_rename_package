package com.victorramayo.iqj.Events;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.victorramayo.iqj.BaseActivity;
import com.victorramayo.iqj.Data.Events;
import com.victorramayo.iqj.Data.News;
import com.victorramayo.iqj.R;
import com.victorramayo.iqj.Utils.Utils;

public class EventsActivity extends BaseActivity {

    public static final String TAG = EventsActivity.class.getName();
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        super.activarToolbar(this,R.id.toolbar);// para el action bar
        super.activarNavigationView(savedInstanceState);//Configuracion de Navigaion View(Menú al costado de la app)
        mProgressBar=(ProgressBar)findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        restCompile();
    }

    private void restCompile(){
        RequestQueue requestQueue = Volley.newRequestQueue(this);// cola de peticiones de Volley
        String URL = Utils.BASE_URL+"eventos_juventud";
        //metodo get para obtener json(se ejecuta en otro hilo)
        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, URL,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ParseodeJSONArray parseo= new ParseodeJSONArray();
                        parseo.execute(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        activarFragment(new ArrayList<News>());
                        Toast.makeText(getApplicationContext(),"Error: "+error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        );
        requestQueue.add(arrayRequest);//paso la peticion a la cola de peticiones
    }

    /**
     * Aqui se extrae y se procesa la informacion para crear nuestra lista de noticias
     * @param response (JSONArray que se recibe de la pgina del Gobierno del Estado)
     * @return ListNews (Lista de objetos News con los valores obtenidos del JSON)
     */
    private ArrayList getJsonService(JSONArray response){
        //response es un JSONArray no es un JSONObject
        ArrayList listEvents=new ArrayList<Events>();
        try{
            for (int i = 0; i < response.length(); i++){
                //Obtengo el objeto title, dentro se encuentra [{"value"}:"titulo"]
                //Obtengo el array dentro de title, dentro se encuentra {"value"}:"titulo"

                //Obtener del JSON el title
                JSONArray titleObj = response.getJSONObject(i).getJSONArray("title");
                JSONObject valueTitle = titleObj.getJSONObject(0);
                String title = valueTitle.getString("value");

                //Obtener del JSON el body
                JSONArray bodyObj = response.getJSONObject(i).getJSONArray("body");
                String body = "";
                if(bodyObj.length()>0){
                    JSONObject valueBody = bodyObj.getJSONObject(0);
                    body = stripHtml(valueBody.getString("value"));// parsear el html y convertirlo a texto normal
                }

                //Obtener del JSON el ubicación
                JSONArray locationObj = response.getJSONObject(i).getJSONArray("field_gen_gmap_address");
                JSONObject valueLocation = locationObj.getJSONObject(0);
                String location = valueLocation.getString("value");

                //Obtener del JSON el imagenes adicionales
                ArrayList<String> AditionalImage = new ArrayList<>();
                JSONArray adImageObj = response.getJSONObject(i).getJSONArray("field_images");
                for(int j=0;j<adImageObj.length();j++) {
                    JSONObject valueAdImage = adImageObj.getJSONObject(j);
                    StringBuilder value = new StringBuilder(valueAdImage.getString("url"));
                    //value.delete(23,30);
                    //value.delete(10,11);
                    AditionalImage.add(String.valueOf(value));
                }

                //Obtener del JSON el imagen principal
                JSONArray mainImageObj = response.getJSONObject(i).getJSONArray("field_mt_slideshow_image");
                JSONObject valueMainImage = mainImageObj.getJSONObject(0);
                String mainImageSub = valueMainImage.getString("url");
                StringBuilder mainImage = new StringBuilder(mainImageSub);
                //mainImage.delete(23,30);
                //mainImage.delete(10,11);

                //agregamos a la lista un nuevo objeto News con los valores obtenidos del JSON
                listEvents.add(new Events (title, mainImage.toString(), body, location));
            }
        }catch (Exception ex){
            Toast.makeText(this, "Error Lista" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }finally{
            return listEvents;
        }
    }

    /**
     *
     * @param html (texto en formato HTML)
     * @return sinTag (el texto sin TAGS de HTML, es decir, texto normaL)
     */
    public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            String sinTag= Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();

            return sinTag.replace("\uFFFC","");
        } else {
            String sinTag= Html.fromHtml(html).toString();

            return sinTag.replace("\uFFFC","");
        }
    }





    /**
     *
     * @param listEvents (Lista con los valores de cada item (Noticia) que se cargaran al RecicleView)
     */
    private void activarFragment(ArrayList listEvents){
        //Verifico si ya esta creado el fragment
        EventsFragment eventsFragment=(EventsFragment) getSupportFragmentManager().findFragmentById(R.id.events_content);

        /**Si no esta creado (agregado) el fragment
         * procedo a cargar el contenerdor del
         * fragment(un lyout) dentro del activity main
         */
        if (eventsFragment==null){
            eventsFragment=EventsFragment.newInstance(listEvents);//paso mi lista al fragment
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.events_content,eventsFragment)
                    .commit();
        }
    }

    private class ParseodeJSONArray extends AsyncTask<JSONArray, Void, Void> {

        @Override
        protected Void doInBackground(JSONArray... response) {
            try {
                TimeUnit.SECONDS.sleep(1);
            }catch (InterruptedException e){
            }
            activarFragment(getJsonService(response[0]));
            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {
            mProgressBar.setVisibility(View.GONE);
        }
    }
}
