package com.victorramayo.iqj.Main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import com.victorramayo.iqj.BaseActivity;
import com.victorramayo.iqj.R;

public class InstitutoActivity extends BaseActivity {

    public TextView text_somos;
    public TextView text_2022;
    public TextView text_mensaje;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instituto); //se carga el layout
        super.activarToolbar(this,R.id.toolbar);// para el action bar
        super.activarNavigationView(savedInstanceState);//Configuracion de Navigaion View(Menú al costado de la app)

        text_somos=(TextView)findViewById(R.id.text_somos);
        text_2022=(TextView)findViewById(R.id.text_2022);
        text_mensaje=(TextView)findViewById(R.id.text_mensaje);

        text_somos.setText("El Instituto Quintanarroense de la Juventud es el espacio donde inician las oportunidades para los jóvenes; además de promover la creación de políticas públicas que benefician de la juventud, diseñamos proyectos que impulsan su desarrollo integral. Al mismo tiempo escuchamos la voz de la juventud por medio de espacios de diálogo, así mantenemos un contacto directo a la vez que conocemos sus ideas, inquietudes y propuestas.\n" +
                "\n" +
                "Nos encargamos también de promover y fomentar en la población joven la sana incorporación de los jóvenes a la vida social, cultural, económica y política, así como fortalecer los factores de integración del tejido social, cohesión familiar, realización individual, superación física e intelectual, valores y las raíces de la cultura e historia milenaria de los quintanarroenses.");

        text_2022.setText("Misión\n" +
                "\n" +
                "Implementar de manera coordinada con las dependencias y entidades del estado, las políticas públicas dirigidas a procurar el desarrollo social y humano de la juventud quintanarroense.\n" +
                "\n" +
                "Visión 2022\n" +
                "\n" +
                "Ser una instancia de juventud vanguardista, reconocida por sus programas y acciones que permitan mejorar la calidad de vida de los jóvenes quintanarroenses, reconociéndolos como actores estratégicos para el desarrollo de Quintana Roo.");
        text_mensaje.setText("Las y los jóvenes tenemos la responsabilidad de construir un mejor Quintana Roo, el estado necesita de nuestra visión y liderazgo, para dar soluciones diferentes, frescas a los retos que enfrentamos día con día. Hoy nuestro compromiso como jóvenes es brindar nuestra energía, trabajo y nuevas ideas para ser agentes estratégicos para el desarrollo de nuestro país.\n" +
                "\n" +
                "Siendo la juventud un factor poblacional tan importante, el Gobernador del Estado creó el Instituto Quintanarroense de la Juventud conquistando así un viejo anhelo de los jóvenes quintanarroenses: contar con una instancia gubernamental capaz de atender la problemática juvenil, así como brindar las oportunidades a este sector poblacional.");
    }

}
