package com.victorramayo.iqj.Events;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.List;

import com.victorramayo.iqj.Data.Events;
import com.victorramayo.iqj.R;


public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder>{

    private List<Events> items;
    private final String TAG="EventsAdapter";

    public static class EventsViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        public ImageView imagen;
        public TextView title;
        public Events events;
        public Context context;
        //public View.OnClickListener itemListener;

        //Recibo el layout de los itmes (item_news) y lo seteo en el viewHolder
        public EventsViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);//activo el lisener on click para cada una de las vistas de la lista
            context=itemView.getContext();
            imagen=(ImageView) itemView.findViewById(R.id.foto_events);
            title=(TextView)itemView.findViewById(R.id.nombre_events);
        }

        /*
         *la accion que se realizara al darle clic al item
         */

        public void onClick(View view) {
            Intent intent=new Intent(context,EventActivity.class);
            intent.putExtra("Object",events);
            context.startActivity(intent);
        }


    }

    //recibo la lista de News y lo iniclaizo dentor del adaptador
    public EventsAdapter(List<Events> items){
        this.items=items;
    }
    // se solicita para operaciones internas del recycle view
    @Override
    public int getItemCount() {
        return items.size();
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<Events> lista){
        items.addAll(lista);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    //Inflo mis itmes del recycle view con el layout item_news
    @Override
    public EventsAdapter.EventsViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_events, viewGroup, false);
        return new EventsViewHolder(v);
    }

    //seteo los datos de mi modelo (clase new), en cada elemnto de mi vista (layout)
    @Override
    public void onBindViewHolder(EventsAdapter.EventsViewHolder viewHolder, final int position) {
        viewHolder.events=items.get(position);
        String imagen = items.get(position).getmImage();
        String title = items.get(position).getmTitle();
        Picasso.get()
                .load(imagen).
                placeholder(R.drawable.image_error)
                .error(R.drawable.image_error).fit()
                .into(viewHolder.imagen);
        //viewHolder.imagen.setDefaultImageResId(R.drawable.image_error);
        //viewHolder.imagen.setErrorImageResId(R.drawable.image_error);
        //ImageLoader imageLoader= NewsSingleton.getInstance(viewHolder.context).getImageLoader();
        //viewHolder.imagen.setImageUrl(imagen,imageLoader);
        viewHolder.title.setText(title);

    }
}