package com.victorramayo.iqj;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;

import com.victorramayo.iqj.Calls.CallsActivity;
import com.victorramayo.iqj.Events.EventsActivity;
import com.victorramayo.iqj.Main.DeveloperActivity;
import com.victorramayo.iqj.Main.InstitutoActivity;
import com.victorramayo.iqj.Main.MainActivity;



/**
 * Heredar de esta clase solo cuan se vaya a utilizar navigation view
 * (esto es, tener un layaot y un navigationView)
 * ademas se debe asignar un toolbar (igual dentro del layout)
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected DrawerLayout mdrawerLayout;
    protected String mdrawerTitle;
    protected Context mcontext;
    protected Activity mActivity;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /**
     * configuracion del Toolbar y action bar de la app
     */
    protected void activarToolbar(Activity activity, int idToolbar){
        mActivity=activity;
        mcontext=mActivity;
        Toolbar toolbar = (Toolbar) findViewById(idToolbar);//Relaciono el toolbar del layout
        setSupportActionBar(toolbar);//establesco al toolbar como la barra de la actividad
        //toolbar.setLogo();//Se direcciona el tipo de imagen que se va ha usar en el toolbar de la app

        //Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        //window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        //window.setStatusBarColor(ContextCompat.getColor(activity,R.color.colorPrimaryDark));

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.ic_main_menu);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setLogo(R.drawable.logo);
        }
    }

    /**
     * Configuracion de Navigaion View(Menú al costado de la app)
     * @param savedInstanceState
     */
    protected void activarNavigationView(Bundle savedInstanceState){
        mdrawerLayout = (DrawerLayout) findViewById(R.id.main_menu_desplegable);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        mdrawerTitle = getResources().getString(R.string.item_news);
        if (savedInstanceState == null) {

        }
    }

    /**
     * Agrego un item listener para seleccionar itesm del navigation view
     * @param navigationView (menu desplegable)
     */
    protected void setupDrawerContent(NavigationView navigationView) {
        Menu menu=navigationView.getMenu();
        for (int i=0;i<menu.size();i++){
            menu.getItem(i).setChecked(false);
        }
        if(mcontext instanceof MainActivity){
            menu.getItem(0).setChecked(true);
        }else if(mcontext instanceof CallsActivity) {
            menu.getItem(1).setChecked(true);
        }

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // Marcar item presionado
                        menuItem.setChecked(true);
                        Intent intent=new Intent();
                        int itemId = menuItem.getItemId();
                        if (itemId == R.id.item_news) {
                            if (!(mcontext instanceof MainActivity)) {
                                intent.setClass(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        } else if (itemId == R.id.item_convocatorias) {
                            if (!(mcontext instanceof CallsActivity)) {
                                intent.setClass(getApplicationContext(), CallsActivity.class);
                                startActivity(intent);
                            }
                        } else if (itemId == R.id.item_eventos) {
                            if (!(mcontext instanceof EventsActivity)) {
                                intent.setClass(getApplicationContext(), EventsActivity.class);
                                startActivity(intent);
                            }
                        } else if (itemId == R.id.item_instituto) {
                            if (!(mcontext instanceof InstitutoActivity)) {
                                intent.setClass(getApplicationContext(), InstitutoActivity.class);
                                startActivity(intent);
                            }
                        } else if (itemId == R.id.item_desarrolladores) {
                            if (!(mcontext instanceof DeveloperActivity)) {
                                intent.setClass(getApplicationContext(), DeveloperActivity.class);
                                startActivity(intent);
                            }
                        }
                        // Crear nuevo fragmento
                        mdrawerLayout.closeDrawers();
                        return true;
                    }
                }
        );
    }
    /**
     * Inflo el menu con el layout menu_main
     * @param menu
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflar el menú; esto agrega elementos a la barra de acción si está presente.
        if (!mdrawerLayout.isDrawerOpen(GravityCompat.START)) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Dentro del layout menu_main
     * relaciono sus items y la accion que deben llevar a cabo
     * en este caso, solo hay una opcion que despliega un toast
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*Manejar clics del elemento de la barra de acción aquí.
        La barra de acciones gestionará automáticamente los clics en el botón Inicio / Arriba,
        siempre que especifique una actividad principal en AndroidManifest.xml.
         */
        switch (item.getItemId()) {
            case android.R.id.home:
                mdrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
