package com.victorramayo.iqj.Calls;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.squareup.picasso.Picasso;
import com.victorramayo.iqj.Data.Calls;
import com.victorramayo.iqj.R;

import java.io.File;
import java.util.ArrayList;


public class CallActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Calls call;
    private ImageView image;
    private PhotoViewAttacher photoAttacher;
    private TextView title;
    private TextView body;
    private LinearLayout layout;
    private final String TAG = CallActivity.class.getSimpleName();
    private final Activity activity = this;
    private final static int REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        activarToolbar();
        call = getIntent().getParcelableExtra("Object");
        image = findViewById(R.id.image_call);
        title = findViewById(R.id.title_call);
        body = findViewById(R.id.body_call);
        inicializarVista();

    }

    protected void activarToolbar() {
        toolbar = findViewById(R.id.toolbar);
        //Relaciono el toolbar del layout
        setSupportActionBar(toolbar);//establesco al toolbar como la barra de la actividad
        //toolbar.setLogo();//Se direcciona el tipo de imagen que se va ha usar en el toolbar de la app

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            //ab.setHomeAsUpIndicator(R.drawable.ic_main_menu);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(R.string.item_convocatorias);
            //ab.setLogo(R.drawable.logo);
        }
    }

    protected void inicializarVista() {
        ArrayList<String> files = call.getmFields();
        title.setText(call.getmTitle());
        body.setText(call.getmBody());
        Picasso.get().load(call.getmImage()).into(image);
        photoAttacher = new PhotoViewAttacher(image);
        photoAttacher.update();


        layout = findViewById(R.id.linear_call);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        int i = 0;
        for (final String urlFile : files) {
            final Button btnTag = new Button(this);
            btnTag.setLayoutParams(layoutParams);
            String filename = Uri.parse(urlFile).getLastPathSegment();
            filename = filename.replace("%20", "");
            String extension = filename.substring(filename.lastIndexOf("."));
            switch (extension) {
                case ".docx":
                    btnTag.setTextColor(this.getResources().getColorStateList(R.color.Black, getTheme()));
                    btnTag.setBackgroundTintList(this.getResources().getColorStateList(R.color.BackgroundTintButtonDoc,getTheme()));
                    break;
                case ".doc":
                    btnTag.setTextColor(this.getResources().getColorStateList(R.color.Black, getTheme()));
                    btnTag.setBackgroundTintList(this.getResources().getColorStateList(R.color.BackgroundTintButtonDoc,getTheme()));
                    break;
                case ".pdf":
                    btnTag.setCompoundDrawablesWithIntrinsicBounds(R.drawable.iconopdf004, 0, 0, 0);
                    btnTag.setTextColor(this.getResources().getColorStateList(R.color.Black,getTheme()));
                    btnTag.setBackgroundTintList(this.getResources().getColorStateList(R.color.BackgroundTintButtonPDF,getTheme()));

                    break;
            }
            /* btnTag.setBackgroundTintList(this.getResources().getColorStateList(R.color.BackgroundTintButton)); */

            btnTag.setGravity(Gravity.LEFT);
            btnTag.setText(Uri.parse(urlFile).getLastPathSegment());
            btnTag.setId(i);
            btnTag.setOnClickListener(v -> {
                try {
                    Uri webpage = Uri.parse(urlFile);
                    Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                    startActivity(myIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, "No application can handle this request. Please install a web browser or check your URL.",  Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            });
            layout.addView(btnTag);
            i++;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }





    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        //TODO entender y modificar para cuadno el usuario dice que no y cuadno pone no volver a pedir permisos
        if (requestCode == REQUEST_CODE) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permisos otorgados correctamente", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "No has concedido permisos de almacenamiento", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
