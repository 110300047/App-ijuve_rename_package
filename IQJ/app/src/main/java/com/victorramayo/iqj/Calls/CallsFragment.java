package com.victorramayo.iqj.Calls;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.victorramayo.iqj.Data.Calls;
import com.victorramayo.iqj.Main.MainActivity;
import com.victorramayo.iqj.R;
import com.victorramayo.iqj.Utils.Utils;

import static com.victorramayo.iqj.Utils.Utils.stripHtml;

public class CallsFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LIST = "Calls List";
    //private final String KEY_RECYCLER_STATE = "recycler_state";
    private RecyclerView mRecyclerView;
    //private static Bundle mBundleRecyclerViewState;
    public static final String TAG= MainActivity.class.getName();
    private CallsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList mCallsList;
    private SwipeRefreshLayout mRefreshLayout;
    private static final String NO_DATE="no date";



    public CallsFragment() {
        // Required empty public constructor
    }

    /**
     *Metodo de fabrica que pone en un bundle a la
     *lista de las noticias
     * @return A new instance of fragment MainFragment.
     */
    public static CallsFragment newInstance(ArrayList arrayList) {
        CallsFragment fragment = new CallsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(LIST,arrayList);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Se inicializa el fragment con la lista de noticias
     * recibida
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCallsList = getArguments().getParcelableArrayList(LIST);
        }
    }

    /**
     * Se crea y configura el recycle view
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root;
        if (mCallsList.size()>0) {
            //Obtengo una instacia (relaciono) de la vista del fragment, asi como de su contenedor
            root = inflater.inflate(R.layout.fragment_calls, container, false);
            //Instacio al recycle view desde elmlayout
            mRecyclerView = (RecyclerView) root.findViewById(R.id.recyclerCalls);
            mRecyclerView.setHasFixedSize(true);//para que se autoajuste
            mLayoutManager = new LinearLayoutManager(container.getContext());//Manejador del recycle que recicla los items

            mRecyclerView.setLayoutManager(mLayoutManager); //Asigno el manager al recycle
            mAdapter = new CallsAdapter(mCallsList);//inicializo mi adaptador
            mRecyclerView.setAdapter(mAdapter); //seteo el adaptador al recycle
            //retorno la vista

            mRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.swipeRefreshCalls);
            mRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            restCompile();
                        }
                    }
            );
            mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.swipe3),
                    getResources().getColor(R.color.swipe2),
                    getResources().getColor(R.color.swipe1),
                    getResources().getColor(R.color.swipe4)
            );
        }else{
            root=inflater.inflate(R.layout.fragment_main_null,container,false);
        }


        return root;
    }

    private void restCompile(){
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());// cola de peticiones de Volley
        String URL = Utils.BASE_URL+"convocatorias_juventud";
        //metodo get para obtener json(se ejecuta en otro hilo)
        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, URL,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ParseodeJSONArray parseo= new ParseodeJSONArray();
                        parseo.execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Error: "+error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        );
        arrayRequest.setShouldCache(false);
        requestQueue.add(arrayRequest);//paso la peticion a la cola de peticiones
    }

    private ArrayList getJsonService(JSONArray response){
        //response es un JSONArray no es un JSONObject
        ArrayList listCalls=new ArrayList<Calls>();
        try{
            for (int i = 0; i < response.length(); i++){
                //Obtengo el objeto title, dentro se encuentra [{"value"}:"titulo"]
                //Obtengo el array dentro de title, dentro se encuentra {"value"}:"titulo"

                //Obtener del JSON el title
                JSONArray titleObj = response.getJSONObject(i).getJSONArray("title");
                JSONObject valueTitle = titleObj.getJSONObject(0);
                String title = valueTitle.getString("value");
                //Obtener del JSON el body
                JSONArray bodyObj = response.getJSONObject(i).getJSONArray("body");

                String body = "";
                if(bodyObj.length()>0){
                    JSONObject valueBody = bodyObj.getJSONObject(0);
                    body = stripHtml(valueBody.getString("value"));// parsear el html y convertirlo a texto normal
                }

                //Obtener del JSON el fecha de inicio
                JSONArray startDateObj = response.getJSONObject(i).getJSONArray("field_gen_fecha_no_hr");
                String startDate= NO_DATE;
                if (startDateObj.length()>0){
                    JSONObject valueStartDate = startDateObj.getJSONObject(0);
                    startDate = valueStartDate.getString("value");
                }

                JSONArray endDateObj = response.getJSONObject(i).getJSONArray("field_fecha_final_no_hr");
                String endDate= NO_DATE;
                if (endDateObj.length()>0){
                    JSONObject valueEndDate = endDateObj.getJSONObject(0);
                    endDate = valueEndDate.getString("value");
                    Date currentTime = Calendar.getInstance().getTime();
                    SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
                    Date dateCall=format.parse(endDate);
                    if (currentTime.after(dateCall)){
                        endDate="Finalizado";
                    }
                }

                //Obtener del JSON el imagen principal
                JSONArray mainImageObj = response.getJSONObject(i).getJSONArray("field_gen_img_header");
                JSONObject valueMainImage = mainImageObj.getJSONObject(0);
                String mainImageSub = valueMainImage.getString("url");
                StringBuilder mainImage = new StringBuilder(mainImageSub);
                //mainImage.delete(26,33);
                //mainImage.delete(7,15);

                JSONArray isNationalObj=response.getJSONObject(i).getJSONArray("field_estatal_o_nacional");
                String isNational="Sin información";
                if(isNationalObj.length()>0) {
                    JSONObject valueIsNational=isNationalObj.getJSONObject(0);
                    isNational = valueIsNational.getString("value");
                    isNational=(isNational=="0"?"Estatal":"Nacional");
                }

                //Obtener del JSON el imagenes adicionales
                ArrayList<String> aditionalFields = new ArrayList<>();
                JSONArray adFieldObj = response.getJSONObject(i).getJSONArray("field_gen_adjuntos");
                for(int j=0;j<adFieldObj.length();j++) {
                    JSONObject valueAdField = adFieldObj.getJSONObject(j);
                    StringBuilder value = new StringBuilder(valueAdField.getString("url"));
                    //value.delete(26,33);
                    //value.delete(7,15);
                    aditionalFields.add(String.valueOf(value));
                }

                listCalls.add(new Calls(title,body,startDate,endDate,mainImage.toString(),isNational,aditionalFields));

            }
        }catch (Exception ex){
            Toast.makeText(getContext(), "Error Lista" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }finally{
            return listCalls;
        }
    }

    private class ParseodeJSONArray extends AsyncTask<JSONArray, Void, ArrayList> {

        @Override
        protected ArrayList doInBackground(JSONArray... response) {
            try {
                TimeUnit.SECONDS.sleep(2);
            }catch (InterruptedException e){
            }

            return getJsonService(response[0]);
        }

        @Override
        protected void onPostExecute(ArrayList list) {
            mAdapter.clear();
            mAdapter.addAll(list);
            mRefreshLayout.setRefreshing(false);
        }
    }
}
