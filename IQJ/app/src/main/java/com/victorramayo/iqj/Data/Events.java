package com.victorramayo.iqj.Data;


import android.os.Parcel;
import android.os.Parcelable;

public class Events  implements Parcelable{

    private String mTitle;
    private String mImage;
    private String mBody;
    private String mUbicacion;

    public Events(String mTitle, String mImage, String mBody, String mUbicacion){
        this.mTitle=mTitle;
        this.mImage=mImage;
        this.mBody=mBody;
        this.mUbicacion=mUbicacion;
    }

    /**
     *Constructor para "desparceabilizar"
     */
    public Events(Parcel parcel){
        this.mTitle=parcel.readString();
        this.mImage=parcel.readString();
        this.mBody=parcel.readString();
        this.mUbicacion=parcel.readString();
    }

    /**
     * crea el objeto News a partir de un parcel
     */
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Events createFromParcel(Parcel entrada) {
            return new Events (entrada);
        }

        public Events[] newArray(int size) {
            return new Events[size];
        }
    };

    //realmente no entendi muy bien para que sirve esto, sin embargo lo solicita la interfaz parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(mTitle);
        parcel.writeString(mImage);
        parcel.writeString(mBody);
        parcel.writeString(mUbicacion);
    }

    //getters y setters
    public String getmTitle() {
        return mTitle;
    }

    public String getmImage() {
        return mImage;
    }

    public String getmBody() {
        return mBody;
    }

    public String getmUbicacion() {
        return mUbicacion;
    }
}
