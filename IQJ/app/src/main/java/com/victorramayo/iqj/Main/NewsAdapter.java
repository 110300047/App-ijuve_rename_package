package com.victorramayo.iqj.Main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;


import com.victorramayo.iqj.Data.News;
import com.victorramayo.iqj.R;


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private List<News> items;
    private final String TAG="NewsAdapter";


    @Override
    public void onViewDetachedFromWindow(@NonNull NewsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    /**
     * La clase ViewHolder sirve para no acceder de manera directa a los elemtos del layout de cada item
     * por seguridad y bueas practicas, el recycleview lo pide por defecto
     */
    public static class NewsViewHolder extends RecyclerView.ViewHolder
            /*implements View.OnClickListener*/{
        public ImageView imagen;
        public TextView title;
        public TextView description;
        public News news;
        public Context context;
        public Button ubicacion;
        public Button more;


        //public View.OnClickListener itemListener;

        //Recibo el layout de los itmes (item_news) y lo seteo en el viewHolder
        public NewsViewHolder(View itemView) {
            super(itemView);
            //itemView.setOnClickListener(this);//activo el lisener on click para cada una de las vistas de la lista
            ubicacion=(Button)itemView.findViewById(R.id.ubicacion);
            context=itemView.getContext();
            imagen=(ImageView) itemView.findViewById(R.id.foto);
            title=(TextView)itemView.findViewById(R.id.nombre);
            description=(TextView)itemView.findViewById(R.id.descripcion);
            more =(Button)itemView.findViewById(R.id.more);
            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(view.getContext(),NewActivity.class);
                    intent.putExtra("Object",news);
                    context.startActivity(intent);
                }
            });
        }

        /*
         *la accion que se realizara al darle clic al item
         */
        /*@Override
        public void onClick(View view) {
            Intent intent=new Intent(context,NewsActivity.class);
            intent.putExtra("Object",news);
            context.startActivity(intent);
        }*/


    }

    //recibo la lista de News y lo iniclaizo dentor del adaptador
    public NewsAdapter(List<News> items){
        this.items=items;
    }
    // se solicita para operaciones internas del recycle view
    @Override
    public int getItemCount() {
        return items.size();
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<News> lista){
        items.addAll(lista);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    //Inflo mis itmes del recycle view con el layout item_news
    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_news, viewGroup, false);
        return new NewsViewHolder(v);
    }

    //seteo los datos de mi modelo (clase new), en cada elemnto de mi vista (layout)
    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder viewHolder, int position) {
        viewHolder.news=items.get(position);
        ArrayList<String> imagen=items.get(position).getmImageAditional();
        String title=items.get(position).getmTitle();
        String body=items.get(position).getmBody();
        Picasso.get()
                .load(imagen.get(0)).
                placeholder(R.drawable.image_error)
                .error(R.drawable.image_error).fit()
                .into(viewHolder.imagen);
        //viewHolder.imagen.setDefaultImageResId(R.drawable.image_error);
        //viewHolder.imagen.setErrorImageResId(R.drawable.image_error);
        //ImageLoader imageLoader= NewsSingleton.getInstance(viewHolder.context).getImageLoader();
        //viewHolder.imagen.setImageUrl(imagen,imageLoader);
        viewHolder.title.setText(title);
        viewHolder.description.setText(body);
        //Onclick para la ubicacion, abre google maps y muestra la ubicacion de Universidad del caribe
        viewHolder.ubicacion.setOnClickListener(view -> {
            String str_location = items.get(position).getmUbicación();
            String map = "http://maps.google.co.in/maps?q=" + str_location +"?z=16&q="+str_location;
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
            view.getContext().startActivity(i);
        });
    }


    @Override
    /*public void onViewDetachedFromWindow(NewsViewHolder viewHolder) {
    }
        super.onViewDetachedFromWindow(NewsViewHolder);
        NewsViewHolder.items.clearAnimation();
    }*/


    public void onViewAttachedToWindow(NewsViewHolder viewHolder) {
        super.onViewAttachedToWindow(viewHolder);
        animateCircularReveal(viewHolder.itemView);
    }

    public void animateCircularReveal(View view) {
        int centerX = 0;
        int centerY = 0;
        int startRadius = 0;
        int endRadius = Math.max(view.getWidth(), view.getHeight());
        Animator animation = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, startRadius, endRadius);
        view.setVisibility(View.VISIBLE);
        animation.start();
    }

    public void animateCircularDelete(final View view, final int list_position) {
        int centerX = view.getWidth();
        int centerY = view.getHeight();
        int startRadius = view.getWidth();
        int endRadius = 0;
        Animator animation = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, startRadius, endRadius);

        animation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(View.INVISIBLE);

            }
        });
        animation.start();
    }


}
