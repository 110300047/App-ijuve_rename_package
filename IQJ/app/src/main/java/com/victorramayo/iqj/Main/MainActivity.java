package com.victorramayo.iqj.Main;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AlertDialog;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.victorramayo.iqj.BaseActivity;
import com.victorramayo.iqj.Data.News;
import com.victorramayo.iqj.R;
import com.victorramayo.iqj.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MainActivity extends BaseActivity {

    //Tag para los Logs
    public static final String TAG= MainActivity.class.getName();
    protected ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //se carga el layout
        super.activarToolbar(this,R.id.toolbar);// para el action bar
        super.activarNavigationView(savedInstanceState);//Configuracion de Navigaion View(Menú al costado de la app)
        mProgressBar=(ProgressBar)findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        restCompile();
        Context c=this;
        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                new AlertDialog.Builder(c)
                        .setTitle("Salir")
                        .setMessage("De verdad deseas salir de la aplicacion?")
                        .setNegativeButton(android.R.string.no, (dialog, id) -> dialog.cancel())
                        .setPositiveButton(android.R.string.yes, (dialog, id) -> {
                            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                                homeIntent.addCategory(Intent.CATEGORY_HOME);
                                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(homeIntent);
                            } else {
                                dialog.cancel();
                                Toast.makeText(getApplicationContext(), "Aplicacion cargando, espere un momento y reintente", Toast.LENGTH_SHORT).show();
                            }
                        }).create().show();
            }
        });
    }

    /**
     * Hacemos petiicon GET a la pagina del Gobierno del Estado
     */

    private void restCompile() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);// cola de peticiones de Volley
        String URL = Utils.BASE_URL+"noticias_juventud";
        //metodo get para obtener json(se ejecuta en otro hilo)

        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, URL,null,
                response -> {
                    ParseodeJSONArray parseo= new ParseodeJSONArray();
                    parseo.execute(response);
                },
                error -> {
                    activarFragment(new ArrayList<News>());
                    Toast.makeText(getApplicationContext(),"Error: "+error.getMessage(),Toast.LENGTH_LONG).show();
                }
        );
        arrayRequest.setShouldCache(false);
        requestQueue.add(arrayRequest);//paso la peticion a la cola de peticiones

    }

    /**
     * Aqui se extrae y se procesa la informacion para crear nuestra lista de noticias
     * @param response (JSONArray que se recibe de la pgina del Gobierno del Estado)
     * @return ListNews (Lista de objetos News con los valores obtenidos del JSON)
     */
    private ArrayList getJsonService(JSONArray response){
        //response es un JSONArray no es un JSONObject
        ArrayList listNews=new ArrayList<News>();
        try{
            for (int i = 0; i < response.length(); i++){
                //Obtengo el objeto title, dentro se encuentra [{"value"}:"titulo"]
                //Obtengo el array dentro de title, dentro se encuentra {"value"}:"titulo"

                //Obtener del JSON el title
                JSONArray titleObj = response.getJSONObject(i).getJSONArray("title");
                JSONObject valueTitle = titleObj.getJSONObject(0);
                String title = valueTitle.getString("value");

                //Obtener del JSON el body
                JSONArray bodyObj = response.getJSONObject(i).getJSONArray("body");
                String body = "";
                if(bodyObj.length()>0){
                    JSONObject valueBody = bodyObj.getJSONObject(0);
                    body = stripHtml(valueBody.getString("value"));// parsear el html y convertirlo a texto normal
                }

                //Obtener del JSON el ubicación
                JSONArray locationObj = response.getJSONObject(i).getJSONArray("field_gen_gmap_address");
                JSONObject valueLocation = locationObj.getJSONObject(0);
                String location = valueLocation.getString("value");

                //Obtener del JSON el imagen principal y agregarla a images
                ArrayList<String> images = new ArrayList<>();
                JSONArray mainImageObj = response.getJSONObject(i).getJSONArray("field_mt_slideshow_image");
                JSONObject valueMainImage = mainImageObj.getJSONObject(0);
                String mainImageSub = valueMainImage.getString("url");
                StringBuilder mainImage = new StringBuilder(mainImageSub);
                //mainImage.insert(53, "/styles/imagen_principal_noticia/public/");
                //mainImage = mainImage.replace("/clone4/","");
                //mainImage.delete(23,30);
                //mainImage.delete(10,11);
                images.add(String.valueOf(mainImage));


                //Obtener del JSON el imagenes adicionales
                JSONArray adImageObj = response.getJSONObject(i).getJSONArray("field_images");
                for(int j=0;j<adImageObj.length();j++) {
                    JSONObject valueAdImage = adImageObj.getJSONObject(j);
                    StringBuilder value = new StringBuilder(valueAdImage.getString("url"));
                    //value.delete(23,30);
                    //value.delete(10,11);
                    images.add(String.valueOf(value));
                }

                //agregamos a la lista un nuevo objeto News con los valores obtenidos del JSON
                listNews.add(new News(title,body,location,images));
            }
        }catch (Exception ex){
            Toast.makeText(this, "Error Lista" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }finally{
            return listNews;
        }
    }

    /**
     *
     * @param html (texto en formato HTML)
     * @return sinTag (el texto sin TAGS de HTML, es decir, texto normaL)
     */
    public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            String sinTag=Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();

            return sinTag.replace("\uFFFC","");
        } else {
            String sinTag= Html.fromHtml(html).toString();

            return sinTag.replace("\uFFFC","");
        }
    }





    /**
     *
     * @param listNews (Lista con los valores de cada item (Noticia) que se cargaran al RecicleView)
     */
    private void activarFragment(ArrayList listNews){
        MainFragment mainFragment=(MainFragment)getSupportFragmentManager().findFragmentById(R.id.main_content);
        //ProgressFragment progressFragment=(ProgressFragment)getSupportFragmentManager().findFragmentById(R.id.main_content);

        /**Si no esta creado (agregado) el fragment
         * procedo a cargar el contenerdor del
         * fragment(un lyout) dentro del activity main
         */

        if (mainFragment==null){
            mainFragment=MainFragment.newInstance(listNews);//paso mi lista al fragment
            getSupportFragmentManager().beginTransaction()
                    //.add(R.id.main_content,mainFragment)
                    .add(R.id.oli,mainFragment)
                    .commit();
        }

    }



    private class ParseodeJSONArray extends AsyncTask<JSONArray, Void, ArrayList> {

        @Override
        protected ArrayList doInBackground(JSONArray... response) {
            try {
                TimeUnit.SECONDS.sleep(1);
            }catch (InterruptedException e){
            }

            return getJsonService(response[0]);
        }

        @Override
        protected void onPostExecute(ArrayList list) {
            activarFragment(list);
            mProgressBar.setVisibility(View.GONE);
        }
    }

}

