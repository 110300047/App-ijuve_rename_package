package com.victorramayo.iqj.Events;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.squareup.picasso.Picasso;

import com.victorramayo.iqj.Data.Events;
import com.victorramayo.iqj.R;


public class EventActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private Events event;
    private ImageView image;
    private PhotoViewAttacher photoAttacher;
    private TextView title;
    private TextView body;
    private LinearLayout layout;
    private Button ubicacion;
    private final String TAG=EventActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        activarToolbar();
        event=(Events) getIntent().getParcelableExtra("Object");
        image=(ImageView)findViewById(R.id.image_event);
        title=(TextView)findViewById(R.id.title_event);
        body=(TextView)findViewById(R.id.body_event);
        ubicacion=(Button)findViewById(R.id.ubicacion_event);
        inicializarVista();
    }

    protected void activarToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);;//Relaciono el toolbar del layout
        setSupportActionBar(toolbar);//establesco al toolbar como la barra de la actividad
        //toolbar.setLogo();//Se direcciona el tipo de imagen que se va ha usar en el toolbar de la app

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            //ab.setHomeAsUpIndicator(R.drawable.ic_main_menu);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(R.string.item_eventos);
            //ab.setLogo(R.drawable.logo);
        }
    }

    protected void inicializarVista(){
        title.setText(event.getmTitle());
        body.setText(event.getmBody());
        ubicacion.setText(event.getmUbicacion());
        Picasso.get().load(event.getmImage()).into(image);
        photoAttacher= new PhotoViewAttacher(image);
        photoAttacher.update();

        ubicacion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String str_location = event.getmUbicacion();
                String map = "http://maps.google.co.in/maps?q=" + str_location +"?z=16&q="+str_location;
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                view.getContext().startActivity(i);
            }
        });


        layout = (LinearLayout) findViewById(R.id.linear_event);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
