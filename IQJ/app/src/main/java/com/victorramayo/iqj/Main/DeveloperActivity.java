package com.victorramayo.iqj.Main;

import android.os.Bundle;

import com.victorramayo.iqj.BaseActivity;
import com.victorramayo.iqj.R;

public class DeveloperActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer); //se carga el layout
        super.activarToolbar(this,R.id.toolbar);// para el action bar
        super.activarNavigationView(savedInstanceState);//Configuracion de Navigaion View(Menú al costado de la app)
    }
}
