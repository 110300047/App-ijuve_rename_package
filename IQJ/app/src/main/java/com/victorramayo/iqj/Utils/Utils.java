package com.victorramayo.iqj.Utils;

import android.os.Environment;
import android.text.Html;

public final class Utils {

    private final static String TAG="Utils";
    public final static String BASE_URL="https://portfolio-api-fohn-dev.fl0.io/";


    private Utils() {
    }


    /**
     *
     * @param html (texto en formato HTML)
     * @return sinTag (el texto sin TAGS de HTML, es decir, texto normaL)
     */
    public static String stripHtml(String html) {

            String sinTag = Html.fromHtml(html, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV).toString();

            return sinTag.replace("\uFFFC", "");

    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }




}
