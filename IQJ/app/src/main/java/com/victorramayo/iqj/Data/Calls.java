package com.victorramayo.iqj.Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Calls implements Parcelable {

    private String mTitle;
    private String mBody;
    private String mStartDate;
    private String mEndDate;
    private String mImage;
    private String mIsNational;
    private ArrayList<String> mFields;


    public Calls(String mTitle, String mBody, String mStartDate,String mEndDate, String mImage,String mIsNational, ArrayList<String> mFields){
        this.mTitle=mTitle;
        this.mBody=mBody;
        this.mStartDate=mStartDate;
        this.mEndDate=mEndDate;
        this.mImage=mImage;
        this.mIsNational=mIsNational;
        this.mFields= mFields;
    }

    /**
     *Constructor para "desparceabilizar"
     */
    public Calls(Parcel parcel){
        this.mTitle=parcel.readString();
        this.mBody=parcel.readString();
        this.mStartDate=parcel.readString();
        this.mEndDate=parcel.readString();
        this.mImage=parcel.readString();
        this.mIsNational=parcel.readString();
        this.mFields=parcel.readArrayList(null);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Calls createFromParcel(Parcel entrada) {
            return new Calls(entrada);
        }

        public Calls[] newArray(int size) {
            return new Calls[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(mTitle);
        parcel.writeString(mBody);
        parcel.writeString(mStartDate);
        parcel.writeString(mEndDate);
        parcel.writeString(mImage);
        parcel.writeString(mIsNational);
        parcel.writeList(mFields);
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmBody() {
        return mBody;
    }

    public String getIsNational() {
        return mIsNational;
    }

    public String getmStartDate() {
        return mStartDate;
    }

    public String getmEndDate() {
        return mEndDate;
    }

    public String getmImage() {
        return mImage;
    }

    public ArrayList<String> getmFields() {
        return mFields;
    }
}
