package com.victorramayo.iqj.Calls;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.victorramayo.iqj.Data.Calls;
import com.victorramayo.iqj.R;


public class CallsAdapter extends RecyclerView.Adapter<CallsAdapter.CallsViewHolder> {

    private List<Calls> items;
    private final String TAG="CallsAdapter";

    public static class CallsViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        public ImageView imagen;
        public TextView title;
        public TextView startDate;
        public TextView endDate;
        public TextView isNational;
        public Calls calls;
        public Context context;
        //public Button ubicacion;
        //public Button more;


        //Recibo el layout de los itmes (item_news) y lo seteo en el viewHolder
        public CallsViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);//activo el lisener on click para cada una de las vistas de la lista
            //ubicacion=(Button)itemView.findViewById(R.id.ubicacion);
            context = itemView.getContext();
            imagen=(ImageView) itemView.findViewById(R.id.image_call);
            title = (TextView) itemView.findViewById(R.id.nombre_calls);
            startDate=(TextView)itemView.findViewById(R.id.fecha_inicio);
            endDate=(TextView)itemView.findViewById(R.id.fecha_fin);
            isNational=(TextView)itemView.findViewById(R.id.estatal_o_nacional);
            //description=(TextView)itemView.findViewById(R.id.descripcion);
            //more =(Button)itemView.findViewById(R.id.more);
            /*more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(view.getContext(),NewsActivity.class);
                    intent.putExtra("Object",news);
                    Toast.makeText(context,"click view",Toast.LENGTH_LONG).show();
                    context.startActivity(intent);
                }
            });*/
        }
        public void onClick(View view) {
            Intent intent=new Intent(context,CallActivity.class);
            intent.putExtra("Object",calls);
            context.startActivity(intent);
        }
    }

    public CallsAdapter(List<Calls> arrayList){items=arrayList;}

    @Override
    public int getItemCount() {
        return items.size();
    }

    /*
    Añade una lista completa de items
     */
    public void addAll(List<Calls> lista){
        items.addAll(lista);
        notifyDataSetChanged();
    }

    /*
    Permite limpiar todos los elementos del recycler
     */
    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    //Inflo mis itmes del recycle view con el layout item_news
    @Override
    public CallsAdapter.CallsViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_calls, viewGroup, false);
        return new CallsAdapter.CallsViewHolder(v);
    }

    //seteo los datos de mi modelo (clase new), en cada elemnto de mi vista (layout)
    @Override
    public void onBindViewHolder(CallsAdapter.CallsViewHolder viewHolder, final int position) {
        viewHolder.calls=items.get(position);
        String imagen=items.get(position).getmImage();
        String title=items.get(position).getmTitle();
        String starDate=items.get(position).getmStartDate();
        String endDate=items.get(position).getmEndDate();
        String isNational=items.get(position).getIsNational();
        Picasso.get()
                .load(imagen).
                placeholder(R.drawable.image_error)
                .error(R.drawable.image_error).fit()
                .into(viewHolder.imagen);
        //viewHolder.imagen.setDefaultImageResId(R.drawable.image_error);
        //viewHolder.imagen.setErrorImageResId(R.drawable.image_error);
        //ImageLoader imageLoader= NewsSingleton.getInstance(viewHolder.context).getImageLoader();
        //viewHolder.imagen.setImageUrl(imagen,imageLoader);
        viewHolder.title.setText(title);
        viewHolder.isNational.setText("Tipo: "+isNational);
        viewHolder.startDate.setText("Inicio: "+starDate);
        viewHolder.endDate.setText("Fin: "+endDate);
        //viewHolder.description.setText(body);
        //Onclick para la ubicacion, abre google maps y muestra la ubicacion de Universidad del caribe
        /*viewHolder.ubicacion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String str_location = items.get(position).getmUbicación();
                String map = "http://maps.google.co.in/maps?q=" + str_location +"?z=16&q="+str_location;
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                view.getContext().startActivity(i);
            }
        });*/

    }
}
