package com.victorramayo.iqj.Main;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.victorramayo.iqj.Data.News;
import com.victorramayo.iqj.R;

public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    public News news;
    private ArrayList<String> items;

    public ViewPagerAdapter (Context context, ArrayList<String> items){
        this.context = context;
        this.items=items;
    }

    @Override
    public int getCount(){
        return items.size();
    }

    @Override
    public boolean isViewFromObject (View view, Object object){
        return view == object;
    }

    @Override
    public Object instantiateItem (ViewGroup container, int position){
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.image_pager,null);
        ((ViewPager) container).addView(view);
        ImageView imageView = (ImageView) view.findViewById(R.id.image_new);

        Picasso.get()
                .load(items.get(position)).
                placeholder(R.drawable.image_error)
                .error(R.drawable.image_error).fit()
                .into(imageView);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object){
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
     }
}
