package com.victorramayo.iqj.Calls;

import android.os.AsyncTask;
import android.os.Bundle;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.victorramayo.iqj.BaseActivity;
import com.victorramayo.iqj.Data.Calls;
import com.victorramayo.iqj.R;
import com.victorramayo.iqj.Utils.Utils;

import static com.victorramayo.iqj.Utils.Utils.stripHtml;

public class CallsActivity extends BaseActivity {

    private final String TAG=CallsActivity.class.getName();
    private static final String NO_DATE="no date";
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calls);
        super.activarToolbar(this,R.id.toolbar);// para el action bar
        super.activarNavigationView(savedInstanceState);//Configuracion de Navigaion View(Menú al costado de la app)
        mProgressBar=(ProgressBar)findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        restCompile();
    }

    private void restCompile(){
        RequestQueue requestQueue = Volley.newRequestQueue(this);// cola de peticiones de Volley
        String URL = Utils.BASE_URL+"convocatorias_juventud";
        //metodo get para obtener json(se ejecuta en otro hilo)
        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, URL,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ParseodeJSONArray parseo= new ParseodeJSONArray();
                        parseo.execute(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        activarFragment(new ArrayList<Calls>());
                        Toast.makeText(getApplicationContext(),"Error: "+error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        );
        arrayRequest.setShouldCache(false);
        requestQueue.add(arrayRequest);//paso la peticion a la cola de peticiones
    }

    /**
     * Aqui se extrae y se procesa la informacion para crear nuestra lista de noticias
     * @param response (JSONArray que se recibe de la pgina del Gobierno del Estado)
     * @return ListNews (Lista de objetos News con los valores obtenidos del JSON)
     */
    private ArrayList getJsonService(JSONArray response){
        //response es un JSONArray no es un JSONObject
        ArrayList listCalls=new ArrayList<Calls>();
        try{
            for (int i = 0; i < response.length(); i++){
                //Obtengo el objeto title, dentro se encuentra [{"value"}:"titulo"]
                //Obtengo el array dentro de title, dentro se encuentra {"value"}:"titulo"

                //Obtener del JSON el title
                JSONArray titleObj = response.getJSONObject(i).getJSONArray("title");
                JSONObject valueTitle = titleObj.getJSONObject(0);
                String title = valueTitle.getString("value");
                //Obtener del JSON el body
                JSONArray bodyObj = response.getJSONObject(i).getJSONArray("body");

                String body = "";
                if(bodyObj.length()>0){
                    JSONObject valueBody = bodyObj.getJSONObject(0);
                    body = stripHtml(valueBody.getString("value"));// parsear el html y convertirlo a texto normal
                }

                //Obtener del JSON el fecha de inicio
                JSONArray startDateObj = response.getJSONObject(i).getJSONArray("field_gen_fecha_no_hr");
                String startDate= NO_DATE;
                if (startDateObj.length()>0){
                    JSONObject valueStartDate = startDateObj.getJSONObject(0);
                    startDate = valueStartDate.getString("value");
                }

                JSONArray endDateObj = response.getJSONObject(i).getJSONArray("field_fecha_final_no_hr");
                String endDate= NO_DATE;
                if (endDateObj.length()>0){
                    JSONObject valueEndDate = endDateObj.getJSONObject(0);
                    endDate = valueEndDate.getString("value");
                    Date currentTime = Calendar.getInstance().getTime();
                    SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
                    Date dateCall=format.parse(endDate);
                    if (currentTime.after(dateCall)){
                        endDate="Finalizado";
                    }
                }

                //Obtener del JSON el imagen principal
                JSONArray mainImageObj = response.getJSONObject(i).getJSONArray("field_gen_img_header");
                JSONObject valueMainImage = mainImageObj.getJSONObject(0);
                String mainImageSub = valueMainImage.getString("url");
                StringBuilder mainImage = new StringBuilder(mainImageSub);
                //mainImage.delete(23,30);
                //mainImage.delete(10,11);

                JSONArray isNationalObj=response.getJSONObject(i).getJSONArray("field_estatal_o_nacional");
                String isNational="Sin información";
                if(isNationalObj.length()>0) {
                    JSONObject valueIsNational=isNationalObj.getJSONObject(0);
                    isNational = valueIsNational.getString("value");
                    isNational=(isNational=="0"?"Estatal":"Nacional");
                }

                //Obtener del JSON el imagenes adicionales
                ArrayList<String> aditionalFields = new ArrayList<>();
                JSONArray adFieldObj = response.getJSONObject(i).getJSONArray("field_gen_adjuntos");
                for(int j=0;j<adFieldObj.length();j++) {
                    JSONObject valueAdField = adFieldObj.getJSONObject(j);
                    StringBuilder value = new StringBuilder(valueAdField.getString("url"));
                    //value.delete(23,30);
                    //value.delete(10,11);
                    aditionalFields.add(String.valueOf(value));
                }

                listCalls.add(new Calls(title,body,startDate,endDate,mainImage.toString(),isNational,aditionalFields));

            }
        }catch (Exception ex){
            Toast.makeText(this, "Error Lista" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }finally{
            return listCalls;
        }
    }

    private void activarFragment(ArrayList listCalls){
        //Verifico si ya esta creado el fragment
        CallsFragment callsFragment=(CallsFragment)getSupportFragmentManager().findFragmentById(R.id.calls_content);

        /**Si no esta creado (agregado) el fragment
         * procedo a cargar el contenerdor del
         * fragment(un lyout) dentro del activity main
         */
        if (callsFragment==null){
            callsFragment=CallsFragment.newInstance(listCalls);//paso mi lista al fragment
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.calls_content,callsFragment)
                    .commit();
        }
    }

    private class ParseodeJSONArray extends AsyncTask<JSONArray, Void, Void> {

        @Override
        protected Void doInBackground(JSONArray... response) {
            try {
                TimeUnit.SECONDS.sleep(1);
            }catch (InterruptedException e){
            }
            activarFragment(getJsonService(response[0]));
            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {
            mProgressBar.setVisibility(View.GONE);
        }
    }
}
