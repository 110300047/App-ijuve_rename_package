package com.victorramayo.iqj.Main;


import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.victorramayo.iqj.Data.News;
import com.victorramayo.iqj.R;
import com.victorramayo.iqj.Utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LIST = "News List";
    //private final String KEY_RECYCLER_STATE = "recycler_state";
    private RecyclerView mRecyclerView;
    //private static Bundle mBundleRecyclerViewState;
    public static final String TAG= MainActivity.class.getName();
    private NewsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList mNewsList;
    private SwipeRefreshLayout refreshLayout;


    public MainFragment() {
        // Required empty public constructor
    }

    /**
     *Metodo de fabrica que pone en un bundle a la
     *lista de las noticias
     * @return A new instance of fragment MainFragment.
     */
    public static MainFragment newInstance(ArrayList arrayList) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(LIST,arrayList);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Se inicializa el fragment con la lista de noticias
     * recibida
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mNewsList = getArguments().getParcelableArrayList(LIST);
        }
    }

    /**
     * Se crea y configura el recycle view
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root;
        if (mNewsList.size()>0) {
            //Obtengo una instacia (relaciono) de la vista del fragment, asi como de su contenedor
            root = inflater.inflate(R.layout.fragment_main, container, false);
            //Instacio al recycle view desde elmlayout
            mRecyclerView = (RecyclerView) root.findViewById(R.id.recyclerNews);
            mRecyclerView.setHasFixedSize(true);//para que se autoajuste
            mLayoutManager = new LinearLayoutManager(container.getContext());//Manejador del recycle que recicla los items
            mRecyclerView.setLayoutManager(mLayoutManager); //Asigno el manager al recycle
            mAdapter = new NewsAdapter(mNewsList);//inicializo mi adaptador
            mRecyclerView.setAdapter(mAdapter); //seteo el adaptador al recycle
            //retorno la vista
            refreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.swipeRefresh);
            refreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            restCompile();
                        }
                    }
            );
            refreshLayout.setColorSchemeColors(getResources().getColor(R.color.swipe3),
                    getResources().getColor(R.color.swipe2),
                    getResources().getColor(R.color.swipe1),
                    getResources().getColor(R.color.swipe4)
            );

        }else{
            root=inflater.inflate(R.layout.fragment_main_null,container,false);
        }


        return root;
    }

    private void restCompile(){
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());// cola de peticiones de Volley
        String URL = Utils.BASE_URL+"noticias_juventud";
        //metodo get para obtener json(se ejecuta en otro hilo)
        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, URL,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ParseodeJSONArray parseo= new ParseodeJSONArray();
                        parseo.execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Error: "+error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        );
        arrayRequest.setShouldCache(false);
        requestQueue.add(arrayRequest);//paso la peticion a la cola de peticiones
    }

    private ArrayList getJsonService(JSONArray response){
        //response es un JSONArray no es un JSONObject
        ArrayList listNews=new ArrayList<News>();
        try{
            for (int i = 0; i < response.length(); i++){
                //Obtengo el objeto title, dentro se encuentra [{"value"}:"titulo"]
                //Obtengo el array dentro de title, dentro se encuentra {"value"}:"titulo"

                //Obtener del JSON el title
                JSONArray titleObj = response.getJSONObject(i).getJSONArray("title");
                JSONObject valueTitle = titleObj.getJSONObject(0);
                String title = valueTitle.getString("value");

                //Obtener del JSON el body
                JSONArray bodyObj = response.getJSONObject(i).getJSONArray("body");
                String body = "";
                if(bodyObj.length()>0){
                    JSONObject valueBody = bodyObj.getJSONObject(0);
                    body = Utils.stripHtml(valueBody.getString("value"));// parsear el html y convertirlo a texto normal
                }

                //Obtener del JSON el ubicación
                JSONArray locationObj = response.getJSONObject(i).getJSONArray("field_gen_gmap_address");
                JSONObject valueLocation = locationObj.getJSONObject(0);
                String location = valueLocation.getString("value");

                ArrayList<String> images = new ArrayList<>();
                JSONArray mainImageObj = response.getJSONObject(i).getJSONArray("field_mt_slideshow_image");
                JSONObject valueMainImage = mainImageObj.getJSONObject(0);
                String mainImageSub = valueMainImage.getString("url");
                StringBuilder mainImage = new StringBuilder(mainImageSub);
                //mainImage.insert(53, "/styles/imagen_principal_noticia/public/");
                //mainImage = mainImage.replace("/clone4/","");
                //mainImage.delete(23,30);
                //mainImage.delete(10,11);
                images.add(String.valueOf(mainImage));


                //Obtener del JSON el imagenes adicionales
                JSONArray adImageObj = response.getJSONObject(i).getJSONArray("field_images");
                for(int j=0;j<adImageObj.length();j++) {
                    JSONObject valueAdImage = adImageObj.getJSONObject(j);
                    StringBuilder value = new StringBuilder(valueAdImage.getString("url"));
                    images.add(String.valueOf(mainImage));
                }

                //agregamos a la lista un nuevo objeto News con los valores obtenidos del JSON
                listNews.add(new News(title,body,location,images));
            }
        }catch (Exception ex){
            Toast.makeText(getContext(), "Error Lista" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }finally{
            return listNews;
        }
    }

    private class ParseodeJSONArray extends AsyncTask<JSONArray, Void, ArrayList> {

        @Override
        protected ArrayList doInBackground(JSONArray... response) {
            try {
                TimeUnit.SECONDS.sleep(2);
            }catch (InterruptedException e){
            }

            return getJsonService(response[0]);
        }

        @Override
        protected void onPostExecute(ArrayList list) {
            mAdapter.clear();
            mAdapter.addAll(list);
            refreshLayout.setRefreshing(false);
        }
    }
}


