package com.victorramayo.iqj.Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Esta clase sirve como modelo para los ekemntos que se van a recibir en el web services
 * en este caso el de noticias
 */
public class News  implements Parcelable{

    private String mTitle;
    private String mBody;
    private String mUbicación;
    private ArrayList<String> mImage;


    public News(String mTitle, String mBody, String mUbicacion, ArrayList<String> mImage){
        this.mTitle=mTitle;
        this.mBody=mBody;
        this.mUbicación=mUbicacion;
        this.mImage=mImage;
    }

    /**
     *Constructor para "desparceabilizar"
     */
    public News(Parcel parcel){
        this.mTitle=parcel.readString();
        this.mBody=parcel.readString();
        this.mUbicación=parcel.readString();
        this.mImage=parcel.readArrayList(null);
    }

    /**
     * crea el objeto News a partir de un parcel
     */
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public News createFromParcel(Parcel entrada) {
            return new News(entrada);
        }

        public News[] newArray(int size) {
            return new News[size];
        }
    };

    //realmente no entendi muy bien para que sirve esto, sin embargo lo solicita la interfaz parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * pasamos nuestros valores dentro de parcel(estamos "parceabilizando")
     * @param parcel obejto parcel
     * @param flags ??
     */
    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(mTitle);
        parcel.writeString(mBody);
        parcel.writeString(mUbicación);
        parcel.writeList(mImage);
    }

    //getters y setters
    public String getmTitle() {
        return mTitle;
    }

    public String getmBody() {
        return mBody;
    }

    public String getmUbicación() {
        return mUbicación;
    }

    public ArrayList<String> getmImageAditional() {
        return mImage;
    }
}
