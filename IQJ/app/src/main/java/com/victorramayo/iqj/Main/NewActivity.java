package com.victorramayo.iqj.Main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.victorramayo.iqj.Data.News;
import com.victorramayo.iqj.R;


public class NewActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private News news;
    private ImageView image;
    private PhotoViewAttacher photoAttacher;
    private TextView title;
    private TextView body;
    private LinearLayout layout;
    private Button ubicacion;
    private ViewPager viewPager;
    private final String TAG=NewActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        activarToolbar();
        news=(News) getIntent().getParcelableExtra("Object");
        image=(ImageView)findViewById(R.id.image_new);
        title=(TextView)findViewById(R.id.title_new);
        body=(TextView)findViewById(R.id.body_new);
        ubicacion=(Button)findViewById(R.id.ubicacion_new);
        viewPager=(ViewPager)findViewById(R.id.viewPager);

        inicializarVista();
    }

    protected void activarToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);;//Relaciono el toolbar del layout
        setSupportActionBar(toolbar);//establesco al toolbar como la barra de la actividad
        //toolbar.setLogo();//Se direcciona el tipo de imagen que se va ha usar en el toolbar de la app

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            //ab.setHomeAsUpIndicator(R.drawable.ic_main_menu);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(R.string.item_news);
            //ab.setLogo(R.drawable.logo);
        }
    }

    protected void inicializarVista(){
        title.setText(news.getmTitle());
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(NewActivity.this, news.getmImageAditional());
        viewPager.setAdapter(viewPagerAdapter);
        Timer timer = new Timer();
        timer.schedule(new MyTimerTask(), 3000, 1500);
        body.setText(news.getmBody());
        ubicacion.setText(news.getmUbicación());

        ubicacion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String str_location = news.getmUbicación();
                String map = "http://maps.google.co.in/maps?q=" + str_location +"?z=16&q="+str_location;
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                view.getContext().startActivity(i);
            }
        });


        layout = (LinearLayout) findViewById(R.id.linear_event);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public class MyTimerTask extends TimerTask{

        @Override
        public void run() {

            NewActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(viewPager.getCurrentItem()==0){
                        viewPager.setCurrentItem(1);
                    }else if(viewPager.getCurrentItem()==1){
                        viewPager.setCurrentItem(2);
                    }else if(viewPager.getCurrentItem()==2){
                        viewPager.setCurrentItem(3);
                    }else if(viewPager.getCurrentItem()==3){
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}
